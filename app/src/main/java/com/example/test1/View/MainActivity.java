package com.example.test1.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.test1.Adapter.MyRecyclerViewAdapter;
import com.example.test1.R;
import com.example.test1.Utils.Contact;
import com.example.test1.Utils.MyApplication;
import com.example.test1.Utils.ViewModelFactory;
import com.example.test1.ViewModel.ContactsViewModel;

import java.util.ArrayList;
import java.util.Iterator;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity implements MyRecyclerViewAdapter.ItemClickListener {
    // Request code for READ_CONTACTS. It can be any number > 0.
    Contact contact;
    MyRecyclerViewAdapter adapter;
    RecyclerView recyclerView;
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;

    @Inject
    ViewModelFactory viewModelFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        contact = new Contact();
        System.out.println("vvvvvvv");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
        } else {
            displayContacts();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                displayContacts();
            } else {
                Toast.makeText(this, "Until you grant the permission, we canot display the names", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onItemClick(View view, int position) {

    }

    public void displayContacts() {
        ContactsViewModel viewModel;
        ((MyApplication) getApplication()).getAppComponent().doInjection(this);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ContactsViewModel.class);
        viewModel.getContactsData().observe(this, new Observer<ArrayList>() {
            @Override
            public void onChanged(ArrayList l) {

                System.out.println(l.size() + " sizeeee  main");

                Iterator itr = l.iterator();
                int count = 0;
                while (itr.hasNext()) {
                    contact = (Contact) itr.next();
                    System.out.println(contact.getContactName() + "    vvvvvv  " + contact.getPhoneNo() + "   vvvv " + count);
                    count++;
                }
                recyclerView = findViewById(R.id.rvAnimals);
                recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                adapter = new MyRecyclerViewAdapter(MainActivity.this, l);
                adapter.setClickListener(MainActivity.this);
                recyclerView.setAdapter(adapter);
            }
        });
    }


}