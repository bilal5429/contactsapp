package com.example.test1.Model;

import android.content.Context;
import java.util.ArrayList;
public class Repository {

    Context context;

    public Repository(Context context) {
        this.context = context;

    }

    public ArrayList fetchContacts() {
        ContactModel contactModel = new ContactModel(context);
        return contactModel.fetchContacts();
    }

}
