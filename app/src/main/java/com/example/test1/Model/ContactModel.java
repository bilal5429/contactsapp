package com.example.test1.Model;


import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;

import com.example.test1.Utils.Contact;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
public class ContactModel {
    Cursor cursor;
    ArrayList contacts;
    Context context;

    ContactModel(Context context)
    {
        this.context = context;
    }

    public ArrayList fetchContacts() {

        ArrayList<Contact> contactsList = new ArrayList<Contact>();

        try {

            cursor = context.getContentResolver()
                    .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
            int Idx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID);
            int nameIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);

            int phoneNumberIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int photoIdIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI);
            cursor.moveToFirst();

            Set<String> ids = new HashSet<>();
            do {
                System.out.println("=====>in while");
                String contactid = cursor.getString(Idx);
                if (!ids.contains(contactid)) {
                    ids.add(contactid);
                    HashMap<String, String> hashMap = new HashMap<String, String>();
                    String name = cursor.getString(nameIdx);
                    String phoneNumber = cursor.getString(phoneNumberIdx);
                    String image = cursor.getString(photoIdIdx);
                    System.out.println("Id--->" + contactid + "Name--->" + name);
                    System.out.println("Id--->" + contactid + "Number--->" + phoneNumber);
                    Contact contact1 = new Contact();
                    contact1.setContactName(name);
                    contact1.setPhoneNo(phoneNumber);
                    contactsList.add(contact1);
                }

            } while (cursor.moveToNext());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return contactsList;
    }

}
