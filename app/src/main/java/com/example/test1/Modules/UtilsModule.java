package com.example.test1.Modules;

import android.content.Context;

import androidx.lifecycle.ViewModelProvider;

import com.example.test1.Model.Repository;
import com.example.test1.Utils.ViewModelFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class UtilsModule {
    @Provides
    @Singleton
    Repository getRepository(Context context) {
        return new Repository(context);
    }

    @Provides
    @Singleton
    ViewModelProvider.Factory getViewModelFactory(Repository myRepository) {
        return new ViewModelFactory(myRepository);
    }
}