package com.example.test1.Utils;

import com.example.test1.View.MainActivity;
import com.example.test1.Modules.AppModule;
import com.example.test1.Modules.UtilsModule;

import javax.inject.Singleton;

import dagger.Component;

    @Component(modules = {AppModule.class, UtilsModule.class})
    @Singleton
    public interface AppComponent {

        void doInjection(MainActivity mainActivity);

    }

