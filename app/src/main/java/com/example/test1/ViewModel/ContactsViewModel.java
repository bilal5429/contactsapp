package com.example.test1.ViewModel;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.test1.Model.Repository;

import io.reactivex.disposables.CompositeDisposable;

public class ContactsViewModel extends ViewModel {

    private Repository repository;
//    private final CompositeDisposable disposables = new CompositeDisposable();
    private final MutableLiveData responseLiveData = new MutableLiveData<>();
    Context context;

    public ContactsViewModel(Repository repository) {
        this.repository = repository;
    }

    public MutableLiveData getContactsData() {

        responseLiveData.setValue(repository.fetchContacts());
        return responseLiveData;
    }

    @Override
    protected void onCleared() {
//        disposables.clear();
    }

}
